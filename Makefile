CC=gcc
CFLAGS= -std=gnu99 -Wall -Wextra -O0 -ggdb3 -fstack-protector -g3
LDFLAGS= -fPIC -pie -Wl,-z,relro,-z,now -Wl,-z,noexecstack 
FRONTSOURCES=src/messages/sender.c src/frontend/frontend.c
BACKSOURCES= src/backend/tcpclient.c src/backend/server.c src/messages/sender.c
BACKTCPSOURCES= src/messages/sender.c src/backend/tcpclient.c
FRONTOBJ=$(FRONTSOURCES:.c=.o)
BACKOBJ=$(BACKSOURCES:.c=.o)
BACKTCPOBJ=$(BACKTCPSOURCES:.c=.o)
FRONTEXEC=frontend
BACKEXEC=backend
BACKTCPEXEC=tcp_backend

all: $(FRONTEXEC) $(BACKEXEC)

$(FRONTEXEC): $(FRONTSOURCES) $(FRONTOBJ)
	mkdir -p build
	$(CC) $(CFLAGS) $(FRONTOBJ) -o build/$@ -luuid
	
$(BACKEXEC): $(BACKOBJ)
	mkdir -p build
	$(CC) $(CFLAGS) $(BACKOBJ) -o build/$@ -luuid

%.o: %.c
	$(CC) $(LDFLAGS) -g3 -c $< -o $@  -luuid

clean:
	rm -rf build
	rm $(FRONTOBJ)
	rm $(BACKOBJ)
	rm $(BACKTCPOBJ)
