#include <stdint.h>
#include <stdio.h> //printf
#include <string.h>    //strlen
#include <sys/socket.h>    //socket
#include <arpa/inet.h> //inet_addr
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <uuid/uuid.h>

#include "../include/messages.h"

#define BUFLEN 34
#define PORT 4242
#define SRV_IP "10.102.185.86"
#define VERSION 1

struct in_addr localInterface;
struct sockaddr_in groupSock;
char groupMcast2[16] = "239.42.4.1";
char yourIP2[16] = "10.102.183.233";

void setArgs(char* gmc, char* ip) {
  strcpy(groupMcast2, gmc);
  strcpy(yourIP2, ip);
}



int sendMsg(char *buf, int size)
{
  /* Create a datagram socket on which to send. */

  int sd = socket(AF_INET, SOCK_DGRAM, 0);
  if(sd < 0)
  {
    perror("Opening datagram socket error");
    exit(1);
  }

  memset((char *) &groupSock, 0, sizeof(groupSock));
  groupSock.sin_family = AF_INET;
  groupSock.sin_addr.s_addr = inet_addr(groupMcast2);
  groupSock.sin_port = htons(4321);

  /* Set local interface for outbound multicast datagrams. */
  /* The IP address specified must be associated with a local, */
  /* multicast capable interface. */
  localInterface.s_addr = inet_addr(yourIP2);
  if(setsockopt(sd, IPPROTO_IP, IP_MULTICAST_IF, (char *)&localInterface, sizeof(localInterface)) < 0)
  {
    perror("Setting local interface error");
    exit(1);
  }
  /* Send a message to the multicast group specified by the*/
  /* groupSock sockaddr structure. */
  /*int datalen = 1024;*/
  if(sendto(sd, buf, size, 0, (struct sockaddr*)&groupSock, sizeof(groupSock)) < 0)
  {
    perror("Sending datagram message error");
  }

  return 0;
}


int sendMsgTCP(int socket, char *buf, int size){
  if (send(socket, buf , size, 0) <0) {
    printf("error send TCP");
    return -1;
  }
  return 0;
}


int setVersionType(char* buf, int version, int type)
{
  return sprintf(buf, "%c%c", (char)version, (char)type);
}

int setUint64(char *buf, int j, uint64_t size)
{
  buf[j] = size >> 56;
  buf[j + 1] = size >> 48;
  buf[j + 2] = size >> 40;
  buf[j + 3] = size >> 32;
  buf[j + 4] = size >> 24;
  buf[j + 5] = size >> 16;
  buf[j + 6] = size >> 8;
  buf[j + 7] = size;
  return 8;
}

int setUint16(char* buf, int j, uint16_t size)
{
  buf[j] = size >> 8;
  buf[j + 1] = size >> 0;
  return 2;
}

int setUint8(char *buf, int j, uint8_t type)
{
  return sprintf(buf + j, "%c", (char)type);
}

int setUuid(char *buf, int j, uuid_t uuid)
{
  int i;
  for(i = 0; i < 16; i++)
  {
    sprintf(buf + j + i, "%c", uuid[i]);
  }
  return 16;
}

int requestFileList(int socket) {
  char *buf = malloc(2);
  setVersionType(buf, 1, 2);
  return sendMsgTCP(socket, buf, 2);
}

int advertiseFile(char *name, uint16_t name_size, uint64_t content_size, uuid_t uuid)
{
  char *buf = malloc(29 + name_size);
  
  int j = 0;

  // Version & Type
  j = setVersionType(buf, 1, 1);

  // Name Size
  j += setUint16(buf, j, name_size);
  
  // Content Size
  j += setUint64(buf, j, content_size);

  // Uuid
  j += setUuid(buf, j, uuid);

  // Digest type
  j += sprintf(buf + j, "%c", (char)0);

  // Name
  j += sprintf(buf + j, "%s", name);

  return sendMsg(buf, 29 + name_size);
}

int requestFile(int socket, uuid_t uuid)
{
  char *buf = malloc(18);
  int j = 0;
  j = setVersionType(buf,1, 4);
  j += setUuid(buf, j, uuid);
  return sendMsgTCP(socket, buf, j);
}

int sendFile(int socket, char* content, uint64_t content_size)
{
  char *buf = malloc(12 + content_size);
  int j = 0;

  j += setVersionType(buf, 1, 5);
  j += setUint8(buf, j, 0);
  j += setUint8(buf, j, 0);
  j += setUint64(buf, j, content_size);
  j += sprintf(buf + j, "%s", content);
  
  return sendMsgTCP(socket, buf, j);
}

int advertiseFileBE(uuid_t uuid)
{
  char *buf = malloc(18);
  int j = 0;

  j += setVersionType(buf, 1, 6);
  j += setUuid(buf, j, uuid);
  
  return sendMsg(buf, 18);
}

int aliveBE()
{
  char *buf = malloc(2);

  setVersionType(buf, 1, 7);
  
  return sendMsg(buf, 2);
}

int discover()
{
  char *buf = malloc(2);

  setVersionType(buf, 1, 8);
  
  return sendMsg(buf, 2);
}
/*
int main(void)
{
  //return discover();
  //return aliveBE();
  //return advertiseFileBE("1234567891234567");
  //return sendFile("totos", 5);
  //return requestFile("1234567891234567");
  //requestFileList();
  
  return advertiseFile("toto", 4, 100, "1234567891234567");
  return advertiseFile("totos", strlen("totos"), 100, "a");
  main2();
  }
*/
