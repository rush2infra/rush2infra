#include <stdint.h>
#include <stdio.h> //printf
#include <string.h>    //strlen
#include <sys/socket.h>    //socket
#include <arpa/inet.h> //inet_addr
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <uuid/uuid.h>
#include <fcntl.h>
#include "../include/messages.h"
#include "../include/struct.h"

char buf[1024]; /* message buffer */
int connect_request_file(char *addr, int port, char *uid, char *name)
{
  int sock;
  struct sockaddr_in server;
  char message[1000];

  //Create socket
  sock = socket(AF_INET, SOCK_STREAM, 0);
  if (sock == -1)
  {
    printf("Could not create socket");
  }

  server.sin_addr.s_addr = inet_addr(addr);
  server.sin_family = AF_INET;
  server.sin_port = htons(4242);

  //Connect to remote server
  if (connect(sock , (struct sockaddr *)&server , sizeof(server)) < 0)
  {
    perror("connect failed.  Error");
    return 1;
  }

  //keep communicating with server
  uuid_t u;
  uuid_parse(uid, u);
  requestFile(sock, u);

  //Receive a reply from the server
  if (read(sock, buf, 1024) < 0)
  //if( recv(sock , buf , 2000 , 0) < 0)
  {
    puts("recv failed");
    return -1;
  }

  close(sock);


  if (buf[1] == 5)
  {
    char *content;
    char *digest;
    struct sending_content_file *scf = content_file(&content, &digest, name);

  }
  else
    printf("NOT RECIVE");
}

int64_t swapLong(void *X) {
  uint64_t x = (uint64_t) X;
  x = (x & 0x00000000FFFFFFFF) << 32 | (x & 0xFFFFFFFF00000000) >> 32;
  x = (x & 0x0000FFFF0000FFFF) << 16 | (x & 0xFFFF0000FFFF0000) >> 16;
  x = (x & 0x00FF00FF00FF00FF) << 8  | (x & 0xFF00FF00FF00FF00) >> 8;
  
  return x;
}


struct sending_content_file *content_file(char **content, char **digest, char *name)
{
  uint64_t size =buf[8] * 4096 + buf[9] * 256 + buf[10] * 16 + buf[11];
  size += buf[7] * 65536 + buf[6] * 1048576 + buf[5] * 16777216 + buf[4] * 268435456;
  struct sending_content_file* scf = (struct sending_content_file*) &buf;

  *content = malloc(scf->content_length * sizeof(char) - sizeof(char));
    int f = open(name, O_WRONLY | O_CREAT, 0666);
    write(f, &buf[12], size - 1);
    close(f); 

  return scf;
}

