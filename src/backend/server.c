/* Receiver/client multicast Datagram example. */
#include <sys/types.h>
#include <string.h>
#include <sys/unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>

#include "../include/struct.h"
#include "../include/messages.h"

struct sockaddr_in localSock;
struct ip_mreq group;
int sd;
int datalen;
char databuf[1024];

char groupMcast[] = "239.42.4.1";
char yourIP[] = "10.102.185.86";

int main(int argc, char *argv[])
{
  if (argc == 3) {
    strcpy(groupMcast, argv[1]);
    strcpy(yourIP, argv[2]);
  }
  /* Create a datagram socket on which to receive. */
  sd = socket(AF_INET, SOCK_DGRAM, 0);
  if(sd < 0)
  {
    perror("Opening datagram socket error");
    exit(1);
  }
  else
    printf("Opening datagram socket....OK.\n");

  /* Enable SO_REUSEADDR to allow multiple instances of this */
  /* application to receive copies of the multicast datagrams. */
  {
    int reuse = 1;
    if(setsockopt(sd, SOL_SOCKET, SO_REUSEADDR, (char *)&reuse, sizeof(reuse)) < 0)
    {
      perror("Setting SO_REUSEADDR error");
      close(sd);
      exit(1);
    }
    else
      printf("Setting SO_REUSEADDR...OK.\n");
  }

  /* Bind to the proper port number with the IP address */
  /* specified as INADDR_ANY. */
  memset((char *) &localSock, 0, sizeof(localSock));
  localSock.sin_family = AF_INET;
  localSock.sin_port = htons(4321);
  localSock.sin_addr.s_addr = INADDR_ANY;
  if(bind(sd, (struct sockaddr*)&localSock, sizeof(localSock)))
  {
    perror("Binding datagram socket error");
    close(sd);
    exit(1);
  }
  else
    printf("Binding datagram socket...OK.\n");

  /* Join the multicast group 226.1.1.1 on the local 203.106.93.94 */
  /* interface. Note that this IP_ADD_MEMBERSHIP option must be */
  /* called for each local interface over which the multicast */
  /* datagrams are to be received. */
  group.imr_multiaddr.s_addr = inet_addr(groupMcast);
  group.imr_interface.s_addr = inet_addr(yourIP);
  if(setsockopt(sd, IPPROTO_IP, IP_ADD_MEMBERSHIP, (char *)&group, sizeof(group)) < 0)
  {
    perror("Adding multicast group error");
    close(sd);
    exit(1);
  }
  else
    printf("Adding multicast group...OK.\n");

  /* Read from the socket. */
  datalen = sizeof(databuf);
  /*if(read(sd, databuf, datalen) < 0)
    {
    perror("Reading datagram message error");
    close(sd);
    exit(1);
    }*/
  struct sockaddr s_addr;
  int len = sizeof(s_addr);
  while (1)
  {
    if (recvfrom(sd, databuf, datalen, 0, &s_addr, &len) < 0)
    {
      perror("Reading datagram message error");
      close(sd);
      exit(1);
    }
    else
    {
      char *ipsrc = inet_ntoa(((struct sockaddr_in *)&s_addr)->sin_addr);
      int portsrc = ntohs(((struct sockaddr_in *)&s_addr)->sin_port);
      printf("Reading datagram message...OK.\n");
      //printf("The message from multicast server is: \"%s\"\n", databuf);
      char *name;
      if (databuf[1] == 1)
      {
        struct message_new_file* mnf = func_mess_new_file(&name);
        char toto[37];
        uuid_unparse(mnf->UUID, toto);
        connect_request_file(ipsrc, portsrc, toto, name);

      }
      else if (databuf[1] == 3)
      {
        recive_list_files();
      }
      else if (databuf[1] == 8)
      {
        aliveBE();
      }

      free(name);
    }
  }
  return 0;
}



struct message_new_file* func_mess_new_file(char** name)
{
  struct message_new_file* mnf = (struct message_new_file*) &databuf;
  mnf->name_length = (mnf->name_length>>8) | (mnf->name_length<<8);
  *name = malloc(mnf->name_length * sizeof(char) - sizeof(char));
  strncpy(*name, &databuf[sizeof(*mnf) - 1], mnf->name_length);

  return mnf;
}
struct list_files* recive_list_files()
{
  //Rempli la 1er struct list_files
  struct list_files* lf = (struct list_files*) &databuf;

  //convert big to littel 32bits
  lf->nb_files = ((lf->nb_files>>24)&0xff) | // move byte 3 to byte 0
    ((lf->nb_files<<8)&0xff0000) | // move byte 1 to byte 2
    ((lf->nb_files>>8)&0xff00) | // move byte 2 to byte
    ((lf->nb_files<<24)&0xff000000);

  //= struct list_files
  int compt = 6;
  for (int i = 0; i < lf->nb_files; i++)
  {
    //copy et feel la struct file_info de databuf
    struct file_info* fi = malloc(sizeof(struct file_info));
    memcpy(fi, &databuf[compt], sizeof(struct file_info));

    //convert big to littel 8bits
    fi->name_length = (fi->name_length>>8) | (fi->name_length<<8);
    printf("longueur du name_file : %d", fi->name_length);

    //convert big to littel 8bits (not sure if not usless) ne nombre de copy
    fi->nb_be = (fi->nb_be>>8) | (fi->nb_be<<8);
    printf("nombre de copie du fichier : %d", fi->nb_be);

    //Trouver le nom du fichier
    compt += 21;
    char *name = malloc(fi->name_length * sizeof(char)/*- sizeof(char)*/);
    memcpy(fi, &databuf[compt], sizeof(struct file_info));
    printf("nom du fichier : %s", name);
    fi->name = name;

    //l'appel a databuf[compt] va depasser le name of file
    compt += fi->name_length;

    for (int j = 0; j < lf->file_list->nb_be; j++)
    {
      //copy et feel la struct be_info de databuf
      struct be_info* bi = malloc(sizeof(struct be_info));
      memcpy(bi, &databuf[compt], sizeof(struct be_info));
      printf("La version ip de BE %d, %d", j+1, bi->addr_type);
      compt += 1;
      //ipv6 = 10 / ipv4 = 2 / none
      if (bi->addr_type == 10)
      {
        //16 (et non 20 sur le schema)
        char *addr = malloc(16);
        memcpy(bi, &databuf[compt], 16);
        printf("addr of the %d : %s", j+1, addr);
        bi->addr = addr;
        compt += 16;
      }
      else if (bi->addr_type == 2)
      {
        char *addr = malloc(8);
        memcpy(bi, &databuf[compt], 8);
        printf("addr of the %d : %s", j+1, addr);
        bi->addr = addr;
        compt += 8;
      }
    }
  }
  return lf;
}
