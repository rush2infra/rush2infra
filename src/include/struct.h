#ifndef STRUCT_H
# define STRUCT_H

#include <uuid/uuid.h>

/*Front-end multicast message advertising a new file*/
struct message_new_file
{
	char version;
	char type;
	unsigned short name_length;
	char content_length[8];
	uuid_t UUID;
	char digest_type;
//	char *name;
//	char *digest_value;
};


/*message donner en input, remplir la struct plut haut*/
struct message_new_file* func_mess_new_file(char** name);

/*
Unicast message requesting the lists of all files
Pas besoin de fonction
*/

struct simple_request
{
	char version;
	char type;
};

struct be_info
{
	char addr_type;
	char *addr;
};


struct file_info
{
	uint16_t name_length;
	char UUID[16];
	uint32_t nb_be;
	char digest_type;
	char *name;
	struct be_info *be_list;
};

struct list_files
{
	char version;
	char type;
	uint32_t nb_files;
	struct file_info *file_list;
};
/*revoie la list des noms des fichier*/
struct list_files* recive_list_files();
/*Unicast message requesting the content of a specific file:*/
struct requested_file
{
	char version;
	char type;
	char UUID[16];
};

void request_content(char *message, struct requested_file mrcf);
/*Unicast message sending the content of a specific file:*/

struct sending_content_file
{
	char version;
	char type;
	char status;
	char digest_type;
	uint64_t content_length;
	char *content;
//	char *digest_value;
};

struct sending_content_file *content_file(char **content, char **digest, char *name);
void dispo_file(char *message, struct requested_file *mdf);

int connect_request_file(char *addr, int port, char *uid, char *name);

#endif
