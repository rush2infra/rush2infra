#ifndef MESSAGES_H_
# define MESSAGES_H_
# include <uuid/uuid.h>

int requestFileList(int socket);

int advertiseFile(char *name, uint16_t name_size, uint64_t content_size, uuid_t uuid);

int requestFile(int socket, uuid_t uuid);

int sendFile(int socket, char* content, uint64_t content_size);

int advertiseFileBE(uuid_t uuid);

int aliveBE();

int discover();

int connect_request_file(char *addr, int port, char *uid, char *name);

void setArgs(char* gmc, char* ip);

#endif /* MESSAGES_H_ */
